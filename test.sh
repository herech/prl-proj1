#!/bin/bash

#pokud neni pocet vstupnich argumentu 1, je to chyba
if [ $# -lt 1 ] || [ $# -gt 1 ];
then
    echo "Chyba: skript přijímá jeden vstupní argument, kterým je kladné číslo > 0";
    exit 1;
#pokud vstupni argument neni cislo, je to chyba
elif [ "$1" -eq "$1" ] 2>/dev/null
then
    #pokud je vstupní argument číslo, které je menší nebo rovno 0, je to chyba (nemá smysl řadit 0 nebo méně vstupních prvků)
    if [ $1 -gt 0 ];
    then
        numbers=$1;
    else
        echo "Chyba: skript přijímá jeden vstupní argument, kterým je kladné číslo > 0";
        exit 1;   
    fi;
else
    echo "Chyba: skript přijímá jeden vstupní argument, kterým je kladné číslo > 0";
    exit 1;
fi;

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -o es es.cpp


#vyrobeni souboru s random cisly
(dd if=/dev/random bs=1 count=$numbers of=numbers) > /dev/null 2>&1

#spusteni
mpirun --prefix /usr/local/share/OpenMPI -np `expr $numbers + 1` es 

#uklid
rm -f es numbers

