/*
 * Popis: Algoritmus enumeration sort na lineárním poli procesorů se sběrnicí
 * Autor: Jan Herec, xherec00
 * Kódování: UTF-8
 */

 #include <mpi.h>
 #include <iostream>
 #include <fstream>
 #include <deque>
 #include <string>
 #include <string.h>
 #include <stdlib.h>

 using namespace std;

 #define TAG 0
 #define TAG_RECEIVE_Z 1 // speciální tag pro komunikaci, kdy se zasílá do registrů z seřazená hodnota
 #define EMPTY -1        // prazdna hodnota v registrech (vstupy mohou byt jen kladne, takze zaporne cislo v registrech znamena ze je registr prazdny)
 #define FALSE 0
 #define TRUE 1

 int main(int argc, char *argv[])
 {
    int numprocs;          // pocet procesoru
    int myid;              // muj rank
    MPI_Status stat;       // struct- obsahuje kod- source, tag, error

    int c_register;        // registr c
    int z_register;        // registr z
    int x_register;        // registr x
    int y_register;        // registr y
    int n;                 // pocet vstupnich/řazených hodnot
    int h;                 // pomocna promenna v cyklu
    int shift_y_register;  // flag jestli se ma shiftovat registr y (tedy jestli prijmu od souseda obsah registru Y)

    // double start, finish;  // Měření složitosti algoritmu

    deque<int> sortedValues;      // seřazené vstupní hodnoty od nejmenší po největší  
    int number;                   // aktuální hodnota pri nacitani ze vstupního souboru
    char number_as_string[10];    // hodnota pri nacitani souboru ale vedena jako řetězec
    int num_of_readed_numbers;    // počet dosud načtených čísel 
    char num_of_readed_numbers_as_string[15]; // počet dosud načtených čísel jako číslo o 4 číslicích (s leading nulami) vedeno jako řetězec

    //MPI INIT
    MPI_Init(&argc,&argv);                          // inicializace MPI 
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);       // zjistíme, kolik procesů běží 
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);           // zjistíme id svého procesu 
 
    // *********** KROK 1: FÁZE INICIALIZACE ******************

    c_register = 1;
    x_register = EMPTY; 
    y_register = EMPTY;
    n = numprocs - 1; // počet řazených hodnot je o jedničku menší než počet procesorů 
    num_of_readed_numbers = 0;

    char input[]= "numbers";      // název vstupního souboru s hodnotami
    fstream fin;                  // stream vstupního souboru
    // proces 0 otevře vstupní soubor s čísly pro čtení 
    if (myid == 0) {
        fin.open(input, ios::in); 
    }
    
    MPI_Request irreq; // specifický typ requestu, který se uplatní jen při čtení zaslaných zpráv přes sběrnici za účelem nahrát x do registru z
    // nemasterová vlákna asynchronně přijmou zprávu s tagem TAG_RECEIVE_Z, tedy přijmou hodnotu do svého registru Z
    // tuto zprávu přijmou někdy v druhé polovině následujícího cyklu for
    if (myid > 0) {
        MPI_Irecv(&z_register, 1, MPI_INT, MPI_ANY_SOURCE, TAG_RECEIVE_Z, MPI_COMM_WORLD, &irreq);
    }
    /* Měření složitosti algoritmu
    MPI_Barrier(MPI_COMM_WORLD); // synchornizujeme procesy
    if (myid == n) {
        start=MPI_Wtime();
    }*/

    // *********** KROK 2: FÁZE KDY SE VSTUPNÍ HODNOTY SEŘADÍ DO ODPOVÍDAJÍCÍCH REGISTRŮ ******************    

    for (int k = 1; k <= 2*n;k++) {
 
        // nastaveni ridici promennhe h        
        if (k <= n) h = 1;
        else h = k - n;

        // pripadna inkrementace registru citace
        if (myid >= h && myid <= n) {
            if ((x_register != EMPTY && y_register != EMPTY) && x_register > y_register) {
                c_register += 1;
            }    
        }
        // provedu pripadny posun regitru Y (zaslu sousedovi)
        if (myid >= h && myid <= n - 1) {
            shift_y_register = FALSE; // defaultne registr Y neposouvam           
            // posunu registr Y
            if (y_register != EMPTY) {
                shift_y_register = TRUE;
                MPI_Send(&shift_y_register, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD); // oznamim sousedovi ze posunu registr Y    
                MPI_Send(&y_register, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD); // poslu sousedovi hodnotu registru Y                
            }    
            // neposunu registr Y
            else {
                MPI_Send(&shift_y_register, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD); // oznamim sousedovi ze neposunu registr Y 
            }
        }

        // pripadne obdrzime registr Y od souseda
        if (myid > h && myid <= n) {
            // zjistim jestli mi soused bude zasilat registr Y a pokud ano, tak jej prijmu
            MPI_Recv(&shift_y_register, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat);             
            if (shift_y_register == TRUE) {
                MPI_Recv(&y_register, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat); 
            }
        }

        // pokud je vstup k dispozici tak jej rozesleme odpovidajicm prcesorum
        if (k <= n) {

            // ridici procesor 0 načte hodnotu ze vstupu a rozešle procesoru 1 a procesoru k
            if(myid == 0){
                number = fin.get(); // naceteme cislo ze vstupu
                // podle toho jestli vypisujeme první nčtené číslo nebo ne, vypíšeme také před tímto číslem mezeru 
                if (num_of_readed_numbers == 0) {
                    cout << number << flush;
                }
                else {
                    cout << " " << number << flush;
                }  

                // přidame natvrdo 4 další číslice ke vstupnímu číslu abychom odlišili stejné hodnoty (číslice představují číslo, které se pro každý vstup inkrementuje)
                sprintf(num_of_readed_numbers_as_string, "%.4d", num_of_readed_numbers);
                sprintf(number_as_string, "%d", number);
                string concatenatedNumbers = string(number_as_string) + string(num_of_readed_numbers_as_string);
                number = atoi(concatenatedNumbers.c_str()); 
                num_of_readed_numbers++;

                MPI_Send(&number, 1, MPI_INT, 1, TAG, MPI_COMM_WORLD); // prvnimu procesoru zasleme vstup              
                MPI_Send(&number, 1, MPI_INT, k, TAG, MPI_COMM_WORLD); // k-temu procesoru zasleme vstupu pres sbernici                           
            }
            // prvni procesor prijme vstup od nulteho procesoru do Y registru
            if(myid == 1){
                MPI_Recv(&y_register, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);                    
            }
            // k-ty procesor prijme vstup od nulteho procesoru do X registru
            if(myid == k){
                MPI_Recv(&x_register, 1, MPI_INT, 0, TAG, MPI_COMM_WORLD, &stat);                         
            }
        }
        // pokud je cas presouvat honoty do registrů Z
        else if (k > n) {
            // poslu obsah registru x danemu procesoru            
            if(myid == k - n) {
                MPI_Send(&x_register, 1, MPI_INT, c_register, TAG_RECEIVE_Z, MPI_COMM_WORLD); // do registru z procesoru jehoz cislo je ulozeno v registru c poslu obsah registru x       
            }

        }

    }
    
    // *********** KROK 3: FÁZE KDY SE HODNOTY V REGISTRECH z VYSUNUJÍ DO FRONTY sortedValues V PROCESORU n ******************  

    // proces 0 zavre vstupni soubor s cisly otevreny pro cteni
    if (myid == 0) {
        fin.close();
    }
    // nemasterová vlákna definitivně dokončí proces přijetí hodnoty do svého registru Z
    if (myid > 0) {
        MPI_Wait(&irreq, &stat);
    }
    
    // vysunujeme registry z do fronty sortedValues
    for (int k = 1; k <= n;k++) {
        
        // n-tý procesor vysouvá svůj registr z do fronty 
        // nemůžeme přímo vypisovat hodnoty z registru Z, protože bychom je vypsali seřazené od největšího po nejmenší
        if (myid == n) {
            sortedValues.push_front(z_register);
        }
        
        // registr z pošleme svému pravému sousedovi
        if (myid >= k && myid <= n-1) {
            MPI_Send(&z_register, 1, MPI_INT, myid + 1, TAG, MPI_COMM_WORLD);    
        }
        // obsah registru z přijmeme od sveho leveho souseda
        if (myid > k && myid <= n) {
            MPI_Recv(&z_register, 1, MPI_INT, myid - 1, TAG, MPI_COMM_WORLD, &stat);   
        }
    }
    /* Měření složitosti algoritmu
    if (myid == n) {
        finish=MPI_Wtime();
        printf("%f\n",(finish-start)*60); 
    }*/

    // *********** KROK 3: FÁZE KDY SE HODNOTY VE FRONTĚ sortedValues VYPÍŠÍ NA VÝSTUP ****************** 

    // nakonec vypíšeme seřazené hodnoty od nejmenšího po největší (vypíše je poslední procesor)
    if (myid == n) {
        deque<int>::iterator v = sortedValues.begin();
        cout << endl;
        while( v != sortedValues.end()) {
            cout << (*v / 10000) << endl; // při výpisu dělíme číslo 1000 tedy odstraníme poslední 4 číslice, které jsme tam na začátku při načtení čísla dodaly, abychom zajistili jeho unikátnost
            v++;
       }
    } 
 
    MPI_Finalize(); 
    return 0;

 }//main

